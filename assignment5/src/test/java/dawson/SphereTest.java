/**
 * Test class for sphere class
 * @author Nathan Bokobza
 * @version 16/10/2023
 */

package dawson;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Unit test for Sphere Class
 */
public class SphereTest {

    /**
     * Returns true if a sphere throws an exception 
     * when given a negative radius.
     */
    @Test
    public void negativeRadiusTest()
    {
        try{
            Sphere s = new Sphere(-5);
            assertFalse( true );
        }
        catch (Exception e)
        {
            assertTrue( true );
        }
    }

    /**
     * Returns true if getter for radius works.
     */
    @Test
    public void getRadiusTest()
    {
        Sphere s = new Sphere(5);
        assertEquals(5, s.getRadius(), 0.00001);
    }


    /**
     * Returns true if volume output is correct.
     */
    @Test
    public void correctVolumeTest()
    {
        int radius = 5;
        Sphere s = new Sphere(radius);
        // double expectedVolume = 4/3*Math.PI*Math.pow(radius, 3);
        double expectedVolume = 523.6;
        assertEquals(s.getVolume(), expectedVolume, 0.1);
    }

    /**
     * Returns true if area output is correct.
     */
    @Test
    public void correctSurfaceAreaTest()
    {
        int radius = 5;
        Sphere s = new Sphere(radius);
        // double expectedArea = 4*Math.PI*Math.pow(radius, 2);
        double expectedArea = 314.16;
        assertEquals(s.getSurfaceArea(), expectedArea, 0.1);
    }
}
