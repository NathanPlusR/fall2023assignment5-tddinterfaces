/**
 * Test class for Cylinder.java
 * @author Mohamed Kamal Aljak
 * @version 16/10/2023
 */
package dawson;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class CylinderTest {
      /**
       * Tests that the method throws an exception if the radius is negative
       */
      @Test(expected = IllegalArgumentException.class)
      public void negativeRadiusValue(){
            Cylinder cylinder = new Cylinder(-5, 1);
      }

      /**
       * Tests that the method throws an exception if the height is negative
       */
      @Test(expected = IllegalArgumentException.class)
      public void negativeHeightValue(){
            Cylinder cylinder = new Cylinder(1, -5);
      }

      /**
       * Tests that the constructor set the correct value for radius and that the getter returns it
       */
      @Test
      public void getRadiusTest(){
            Cylinder cylinder = new Cylinder(1, 5);
            assertEquals(1, cylinder.getRadius(), 0);
      }

      /**
       * Tests that the constructor set the correct value for height and that the getter returns it
       */
      @Test
      public void getHeightTest(){
            Cylinder cylinder = new Cylinder(1, 5);
            assertEquals(5, cylinder.getHeight(), 0);
      }

      /**
       * Tests that the getVolume method returns correct output given valid inputs
       */
      @Test
      public void getVolumeTest(){
            Cylinder cylinder = new Cylinder(3, 5);
            assertEquals(141.37, cylinder.getVolume(), 0.01);
      }

      
      /**
       * Tests that the getSurfaceArea method returns correct output given valid inputs
       */
      @Test
      public void getSurfaceAreaTest(){
            Cylinder cylinder = new Cylinder(3, 5);
            assertEquals(150.8, cylinder.getSurfaceArea(), 0.01);
      }
}
