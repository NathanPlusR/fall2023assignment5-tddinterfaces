/**
 * Cylinder Class
 * @author Nathan Bokobza
 * @version 17/10/2023
 */

package dawson;

public class Cylinder implements Shape3d{
    
    private double radius;
    private double height;

    /**
     * @param radius is the radius of the cylinder
     * @param height is the height of the cylinder
     */
    public Cylinder(double radius, double height)
    {
        if (radius < 0 || height < 0)
        {
            throw new IllegalArgumentException("Cannot have a negative radius or height for a cylinder!");
        }
        this.radius = radius;
        this.height = height;
    }

    /**
     * @return returns radius of cylinder
     */
    public double getRadius()
    {
        return this.radius;
    }

    /**
     * @return returns height of cylinder
     */
    public double getHeight()
    {
        return this.height;
    }

    /**
     * @return returns volume of cylinder
     */
    public double getVolume() {
        double volume = Math.PI * Math.pow(this.radius, 2) * this.height;
        return volume;
    }

    /**
     * @return returns area of cylinder
     */
    public double getSurfaceArea() {
        double area = 2 * Math.PI * Math.pow(this.radius, 2) + 2 * Math.PI * this.radius * this.height;
        return area;
    }
    
}
