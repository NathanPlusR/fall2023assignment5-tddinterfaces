/**
 * This class creates sphere shapes and performs calculations based on the sphere's values
 * @author Mohamed Kamal Aljak
 * @version 17/20/2023
 */
package dawson;

public class Sphere implements Shape3d{
      private double radius;

      /**
       * @param radius - The radius of the sphere 
       */
      public Sphere(double radius){
            if(radius < 0) throw new IllegalArgumentException();
            this.radius = radius;
      }

      /**
       * Getter method for radius instance variable
       * @return instance variable radius
       */
      public double getRadius(){
            return this.radius;
      }

      /**
       * Calculates the volume of a sphere given it's radius
       * @return the volume of the sphere
       */
      @Override
      public double getVolume() {
            double volume = 4.0/3.0 * Math.PI * Math.pow(radius, 3);
            return volume;
      }

      /**
       * Calculates the surface area of a sphere given its radius
       * @return the surface area of the sphere
       */
      @Override
      public double getSurfaceArea() {
            double surfaceArea = 4 * Math.PI * Math.pow(radius, 2);
            return surfaceArea;
      }  
}
